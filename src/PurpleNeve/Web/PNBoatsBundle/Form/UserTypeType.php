<?php

namespace PurpleNeve\Web\PNBoatsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserTypeType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PurpleNeve\Web\PNBoatsBundle\Entity\UserType'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'purpleneve_web_pnboatsbundle_usertype';
    }
}
