<?php
namespace PurpleNeve\Web\PNBoatsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\SecurityContext;

class DefaultController extends Controller {

    /**
     * loginAction - Default page for the control panel, all users are directed here to authenticate prior to accessing anything within the system.
     *
     * @param \Request $request
     */
    public function loginAction(Request $request)
    {
        $session = $request->getSession();

        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                SecurityContext::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        $authForm = $this->get('form.factory')->createNamedBuilder(null, 'form', array())
            ->setAction('/cp/login/process')
            ->setMethod('POST')
            ->add(
                '_username',
                'email',
                array(  'attr' =>
                    array(  'size' => 'small',
                            'placeholder' => $this->get('translator')->trans('Email Address'),
                            'name' => '_username',
                            'value' => $session->get(SecurityContext::LAST_USERNAME)
                    ),
                    'label' => /** @Ignore */false
                )
            )
            ->add(
                '_password',
                'password',
                array(  'attr' =>
                    array(  'size' => 'small',
                            'placeholder' => $this->get('translator')->trans('Password'),
                            'name' => '_password'
                    ),
                    'label' => /** @Ignore */false
                )
            )
            ->add(
                'authUser',
                'submit',
                array(  'attr' =>
                    array(  'size' => 'small',
                            'class' => 'btn btn-danger',
                    ),
                    'label' => 'Login'
                )
            )->getForm();

        return $this->render('PNBoatsBundle:Default:login.html.twig', array(
            'login_form'    => $authForm->createView(),
            'error'         => $error
        ));
    }

    /* forgotPasswordAction - Recover users lost password via email
     * @param \Request $request
     */
    public function forgotPasswordAction(Request $request)
    {
        $error = null;
        $success = null;

        if ($request->getMethod() === "POST") {
            $userEmail = $request->get('_email');

            $user = $this->getDoctrine()->getRepository('PNBoatsBundle:User');
            $user = $user->findOneBy(array('email' => $userEmail));

            if ($user) {
                $message = \Swift_Message::newInstance()
                    ->setSubject('Password Recovery')
                    ->setFrom('noreply@navalbuddy.com')
                    ->setTo($userEmail)
                    ->setBody($this->renderView('PNBoatsBundle:Email:forgotPassword.txt.twig'), 'text/html');

                $this->get('mailer')->send($message);
                $success = $this->get('translator')->trans('An email was sent to ') . $userEmail;
            } else {
                $error = "No user is associated with that email";
            }
        }

        $forgotForm = $this->get('form.factory')->createNamedBuilder(null, 'form', array())
            ->setAction('')
            ->setMethod('POST')
            ->add(  '_email',
                    'email',
                    array(
                        'attr' => array(
                            'size' => 'small',
                            'placeholder' => $this->get('translator')->trans('Enter your email address'),
                            'name' => '_email'
                        ),
                        'label' => /** @Ignore */false
                    ))
            ->add(  'authUser',
                    'submit',
                    array(
                        'attr' => array(
                            'size' => 'small'
                        ),
                    'label' => 'Reset My Password'
                    ))
            ->getForm();

        return $this->render('PNBoatsBundle:Default:forgotPassword.html.twig', array(
            'recovery_form' => $forgotForm->createView(),
            'error'         => $error,
            'success'       => $success
        ));
    }

    public function checkLoginAction()
    {
        throw new NotFoundHttpException("We should not be calling this, the firewall is not intercepting the call");
    }

    public function logoutAction()
    {
        throw new NotFoundHttpException("We should not be calling this, the firewall is not intercepting the call");
    }

    public function registerAction()
    {

    }
}
